pub mod client;
use core::fmt;
use std::{fmt::Formatter, io::BufRead, iter::Map};

pub use client::*;

#[derive(Debug)]
pub enum NexLine<'a> {
    Link(&'a str, &'a str, bool),
    Text(&'a str),
}

impl<'a> fmt::Display for NexLine<'a> {
    // Converts GeminiLine back into its gemtext representation.
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        match *self {
            NexLine::Text(text) => {
                write!(f, "{}", text)
            },
            NexLine::Link(url, title, has_title) => {
                if has_title {
                    write!(f, "=> {} {}", url, title)
                } else {
                    write!(f, "=> {}", url)
                }
            }
        }
    }
}

// Returns true on EOF or Error
pub fn parse_line_from_reader<'a, T>(r: &mut T, buf: &'a mut String) -> (Option<NexLine<'a>>, bool) where T: BufRead {
    buf.clear();
    match r.read_line(buf) {
        Ok(0) => {
            (Some(parse_line(buf.trim_end())), true)
        }
        Ok(_) => {
            (Some(parse_line(buf.trim_end())), false)
        }
        Err(_) => {
            (None, true)
        }
    }
}

pub fn parse_iter<'a>(s: &'a str) -> Map<std::str::Lines<'_>, fn(&'a str) -> NexLine<'a>> {
    return s.lines().map(|line| {
       parse_line(line)
    });
}

pub fn parse_line(line: &str) -> NexLine {
    return if line.starts_with("=>") {
        let (url, title, has_title) = parse_link(line);
        NexLine::Link(url, title, has_title)
    } else {
        NexLine::Text(&line[..])
    };
}

pub(crate) fn parse_link(s: &str) -> (&str, &str, bool) {
    let trimmed = s.trim_start_matches("=>").trim_start();
    return match trimmed.split_once([' ', '\t']) {
        Some((before, after)) => (before, after.trim_start(), true),
        None => (&trimmed[..], &trimmed[trimmed.len()..].trim_start(), false),
    };
}
