use std::error::Error;
use std::fmt::Write as fmtWrite;
use std::io::{self, BufRead, BufReader, Write};
use std::net::TcpStream;

use crate::nex::parse_line;

pub struct Client {
}

impl Client {
    pub fn fetch(&self, address: &str, path: &str) -> Result<BufReader<TcpStream>, io::Error> {
        let mut address_to_use = String::new();
        if let Some((host, port)) = address.split_once(':') {
            match idna::domain_to_ascii(host) {
                Ok(domain) => {
                    address_to_use.write_str(&domain).expect("Error.");
                }
                Err(_) => todo!(),
            }

            if port == "" {
                address_to_use.write_str("1900").expect("Error appending port.");
            }
        } else {
            match idna::domain_to_ascii(address) {
                Ok(domain) => {
                    address_to_use.write_str(&domain).expect("Error.");
                }
                Err(_) => todo!(),
            }

            address_to_use.write_str(":1900").expect("Error appending port.");
        }

        //if let Ok(mut stream) = 
        match TcpStream::connect(address_to_use) {
            Ok(mut stream) => {
                stream.write(path.as_ref()).expect("Error sending request.");
                stream.write("\n".as_ref()).expect("Error sending request.");
                stream.flush().expect("Error sending request.");

                let reader = BufReader::new(stream.try_clone().expect("clone failed..."));
                Ok(reader)
            } 
            Err(err) => {
                Err(err)
            }
        }
    }
}
