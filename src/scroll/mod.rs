pub mod client;

use std::{fmt, fmt::Formatter};
use std::{io::BufRead};
use std::{iter::Map};

#[derive(Debug)]
pub enum ScrollLine<'a> {
    Heading(&'a str, usize), // text, level
    UnorderedBullet(&'a str, usize), // text, level
    OrderedBullet(&'a str, &'a str, usize), // label, text, level
    Quote(&'a str, usize), // text, level
    ThematicBreak,
    Link(&'a str, &'a str, bool), // url, title, has_title
    PreformatToggle(&'a str), // text
    Text(&'a str), // text
    None,
}

impl<'a> fmt::Display for ScrollLine<'a> {
    // Converts a ScrollLine back into its gemtext representation.
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        match *self {
            ScrollLine::Heading(text, level) => {
                write!(f, "{} {}", "#".repeat(level), text)
            }
            ScrollLine::UnorderedBullet(text, level) => {
                write!(f, "{} {}", "*".repeat(level), text)
            }
            ScrollLine::OrderedBullet(label, text, level) => {
                write!(f, "{} {}. {}", "*".repeat(level), label, text)
            }
            ScrollLine::Quote(text, level) => {
                write!(f, "{} {}", ">".repeat(level), text)
            }
            ScrollLine::ThematicBreak => {
                write!(f, "---")
            }
            ScrollLine::Link(url, title, has_title) => {
                if has_title {
                    write!(f, "=> {} {}", url, title)
                } else {
                    write!(f, "=> {}", url)
                }
            }
            ScrollLine::PreformatToggle(text) => {
                write!(f, "```{}", text)
            }
            ScrollLine::Text(text) => {
                write!(f, "{}", text)
            }
            ScrollLine::None => {
                write!(f, "")
            }
        }
    }
}

// Returns true on EOF or Error
pub fn parse_line_from_reader<'a, T>(r: &mut T, buf: &'a mut String) -> (Option<ScrollLine<'a>>, bool) where T: BufRead {
    buf.clear();
    match r.read_line(buf) {
        Ok(0) => {
            (Some(parse_line(buf.trim_end())), true)
        }
        Ok(_) => {
            (Some(parse_line(buf.trim_end())), false)
        }
        Err(_) => {
            (None, true)
        }
    }
}

pub fn parse_iter<'a>(s: &'a str) -> Map<std::str::Lines<'_>, fn(&'a str) -> ScrollLine<'a>> {
    return s.lines().map(|line| {
        parse_line(line)
    });
}

pub fn parse_line(line: &str) -> ScrollLine {
    return if line.starts_with("####") {
        ScrollLine::Heading((&line[..]).trim_start_matches("####").trim_start(), 4)
    } else if line.starts_with("###") {
        ScrollLine::Heading(line.trim_start_matches("###").trim_start(), 3)
    } else if line.starts_with("##") {
        ScrollLine::Heading(line.trim_start_matches("##").trim_start(), 2)
    } else if line.starts_with("#") {
        ScrollLine::Heading(line.trim_start_matches("#").trim_start(), 1)
    } else if line.starts_with("* ") || line.starts_with("*\t") || line.starts_with("** ") || line.starts_with("**\t") || line.starts_with("*** ") || line.starts_with("***\t")|| line.starts_with("**** ") || line.starts_with("****\t") {
        // Note that this works because we know that asterisk is one byte in UTF-8.
        let Some(level) = line.find(|c| {
            return c != '*'
        }) else { todo!() };
        let line = line[level..].trim_start();
        let (ordered, label, text) = bullet_is_ordered(line);
        if ordered {
            ScrollLine::OrderedBullet(label, text, level)
        } else {
            ScrollLine::UnorderedBullet(line, level)
        }
    } else if line.starts_with('>') {
        // Note that this only works because we know that '>' is a one-byte codepoint in UTF-8.
        let Some(level) = line.find(|c| {
            return c != '>'
        }) else { todo!() };
        ScrollLine::Quote(line[level..].trim_start(), level)
    } else if line == "---" || line == "---\n" || line == "---\r\n" {
        ScrollLine::ThematicBreak
    } else if line.starts_with("=>") {
        let (url, title, has_title) = crate::gemini::parse_link(line);
        ScrollLine::Link(url, title, has_title)
    } else if line.starts_with("```") {
        //in_preformat = !in_preformat;
        ScrollLine::PreformatToggle(line.trim_start_matches("```").trim_start())
    } else {
        ScrollLine::Text((&line[..]))
    };
}

// Pass in a line string slice with the bullet prefix (e.g., "* ") trimmed. Returns whether it's an
// ordered or unordered bullet, the label, and the text. If unordered, the label is an empty slice.
fn bullet_is_ordered(line: &str) -> (bool, &str, &str) {
    let mut text = line.trim_start();
    let Some(label_end) = text.find(|c: char| {
        return !c.is_digit(10) && c != '.'
    }) else { todo!() };

    return if label_end == 0 { // Unordered
        (false, &text[0..0], text)
    } else { // Ordered
        let label = &text[..label_end].trim_end_matches(".");
        text = &text[label_end..];
        (true, label, text)
    }
}

fn parse_link(s: &str) -> (&str, &str, bool) {
    let trimmed = s.trim_start_matches("=>").trim_start();
    return match trimmed.split_once([' ', '\t']) {
        Some((before, after)) => (before, after.trim_start(), true),
        None => (&trimmed[..], &trimmed[trimmed.len()..].trim_start(), false),
    };
}

pub enum LinkRelation<'a> {
    Negative(&'a str),
    Neutral(&'a str),
    Positive(&'a str),
    None,
}

// Returns the title without the relation label, the relation label, and an integer of whether it's
// a neutral, positive, or negative relation.
pub fn parse_link_title_relation(title: &str) -> (&str, LinkRelation) {
    return match title.split_once('[') {
        Some((before, after)) => {
            let mut label = after.trim_start_matches('[').trim().trim_end_matches(']');
            if label.starts_with('+') {
                label = label.trim_start_matches('+');
                (before, LinkRelation::Positive(label))
            } else if label.starts_with('-') {
                label = label.trim_start_matches('-');
                (before, LinkRelation::Negative(label))
            } else {
                (before, LinkRelation::Neutral(label))
            }
        },
        None => (title, LinkRelation::None)
    }
}
