use std::io::{BufReader, Write};
use std::net::TcpStream;
use super::parse_line_from_reader;

pub struct Client {
}

impl Client {
    pub fn fetch(&self, address: &str) {
        if let Ok(mut stream) = TcpStream::connect("192.168.0.60:1900") {
            match stream.write("/\n".as_ref()) {
                Ok(_) => {}
                Err(_) => {}
            }
            let mut reader = BufReader::new(stream.try_clone().expect("clone failed..."));
            let mut buf = String::new();
            loop {
                match parse_line_from_reader(&mut reader, &mut buf) {
                    (None, _) => {}
                    (Some(_), true) => {
                        break
                    }
                    (Some(line), false) => {
                        println!("{}", line.to_string())
                    }
                }
            }
        } else {}
    }
}