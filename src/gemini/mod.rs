pub mod client;
pub use client::*;

use std::{fmt, fmt::Formatter};
use std::{io::BufRead, io::Read};
use std::{iter::Map};

// Supports Gemtext, Spartan, and Scroll.
#[derive(Debug)]
pub enum GeminiLine<'a> {
    Heading(&'a str, usize),
    Link(&'a str, &'a str, bool),
    PromptLink(&'a str, &'a str, bool),
    Bullet(&'a str),
    Quote(&'a str),
    PreformatToggle(&'a str),
    Text(&'a str),
    None,
}

impl<'a> fmt::Display for GeminiLine<'a> {
    // Converts GeminiLine back into its gemtext representation.
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        match *self {
            GeminiLine::Heading(text, level) => {
                write!(f, "{} {}", "#".repeat(level), text)
            }
            GeminiLine::Link(url, title, has_title) => {
                if has_title {
                    write!(f, "=> {} {}", url, title)
                } else {
                    write!(f, "=> {}", url)
                }
            }
            GeminiLine::PromptLink(url, title, has_title) => {
                if has_title {
                    write!(f, "=: {} {}", url, title)
                } else {
                    write!(f, "=: {}", url)
                }
            }
            GeminiLine::Bullet(text) => {
                write!(f, "* {}", text)
            }
            GeminiLine::Quote(text) => {
                write!(f, "> {}", text)
            }
            GeminiLine::PreformatToggle(text) => {
                write!(f, "```{}", text)
            }
            GeminiLine::Text(text) => {
                write!(f, "{}", text)
            }
            GeminiLine::None => {
                write!(f, "")
            }
        }
    }
}

// Returns true on EOF or Error
pub fn parse_line_from_reader<'a, T>(r: &mut T, buf: &'a mut String) -> (Option<GeminiLine<'a>>, bool) where T: BufRead {
    buf.clear();
    match r.read_line(buf) {
        Ok(0) => {
            (Some(parse_line(buf.trim_end())), true)
        }
        Ok(_) => {
            (Some(parse_line(buf.trim_end())), false)
        }
        Err(_) => {
            (None, true)
        }
    }
}

pub fn parse_iter<'a>(s: &'a str) -> Map<std::str::Lines<'_>, fn(&'a str) -> GeminiLine<'a>> {
    return s.lines().map(|line| {
       parse_line(line)
    });
}

pub fn parse_line(line: &str) -> GeminiLine {
    return if line.starts_with("####") {
        GeminiLine::Heading((&line[..]).trim_start_matches("####").trim_start(), 4)
    } else if line.starts_with("###") {
        GeminiLine::Heading(line.trim_start_matches("###").trim_start(), 3)
    } else if line.starts_with("##") {
        GeminiLine::Heading(line.trim_start_matches("##").trim_start(), 2)
    } else if line.starts_with("#") {
        GeminiLine::Heading(line.trim_start_matches("#").trim_start(), 1)
    } else if line.starts_with("=>") {
        let (url, title, has_title) = parse_link(line);
        GeminiLine::Link(url, title, has_title)
    } else if line.starts_with("=:") {
        let (url, title, has_title) = parse_prompt_link(line);
        GeminiLine::PromptLink(url, title, has_title)
    } else if line.starts_with("* ") || line.starts_with("*\t") {
        GeminiLine::Bullet(line.trim_start_matches(&['*', ' ', '\t']).trim_start())
    } else if line.starts_with('>') {
        GeminiLine::Quote(line.trim_start_matches('>').trim_start())
    } else if line.starts_with("```") {
        //in_preformat = !in_preformat;
        GeminiLine::PreformatToggle(line.trim_start_matches("```").trim_start())
    } else {
        GeminiLine::Text(&line[..])
    };
}

pub(crate) fn parse_link(s: &str) -> (&str, &str, bool) {
    let trimmed = s.trim_start_matches("=>").trim_start();
    return match trimmed.split_once([' ', '\t']) {
        Some((before, after)) => (before, after.trim_start(), true),
        None => (&trimmed[..], &trimmed[trimmed.len()..].trim_start(), false),
    };
}

pub(crate) fn parse_prompt_link(s: &str) -> (&str, &str, bool) {
    let trimmed = s.trim_start_matches("=:").trim_start();
    return match trimmed.split_once([' ', '\t']) {
        Some((before, after)) => (before, after.trim_start(), true),
        None => (trimmed, &trimmed[trimmed.len()..].trim_start(), false),
    };
}

pub enum LinkRelation<'a> {
    Negative(&'a str),
    Neutral(&'a str),
    Positive(&'a str),
    None,
}

// Returns the title without the relation label, the relation label, and an integer of whether it's
// a neutral, positive, or negative relation.
pub fn parse_link_title_relation(title: &str) -> (&str, LinkRelation) {
    return match title.split_once('[') {
        Some((before, after)) => {
            let mut label = after.trim_start_matches('[').trim().trim_end_matches(']');
            if label.starts_with('+') {
                label = label.trim_start_matches('+');
                (before, LinkRelation::Positive(label))
            } else if label.starts_with('-') {
                label = label.trim_start_matches('-');
                (before, LinkRelation::Negative(label))
            } else {
                (before, LinkRelation::Neutral(label))
            }
        },
        None => (title, LinkRelation::None)
    }
}

// Parses emphasis, strong, and monospace inline markup using Markdown syntax (*italic*, **bold**,
// and `monospace`).
pub fn parse_inline_markup(text: &str) {

}
