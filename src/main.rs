use std::io::{BufRead, BufReader};
use rustyline::{error::ReadlineError, DefaultEditor};
use scroll_term_rust::{gemini, nex::{self, parse_iter, parse_line_from_reader, NexLine}};

fn main() {
    let mut rl = DefaultEditor::new().expect("Error");
    
    'outer: loop {
        let readline = rl.readline("> ");
        match readline {
            Ok(line) => {
                rl.add_history_entry(line.as_str()).expect("Error adding to history.");
            },
            Err(ReadlineError::Interrupted) => { // Ctrl+C
                break 'outer
            },
            Err(ReadlineError::Eof) => { // Ctrl+D
                break 'outer
            },
            Err(_) => todo!(),
        }
    }
}

fn goto_nex_page() {
    let client = nex::Client{};
    let mut reader = client.fetch("auragem.letz.dev", "/").expect("Failed.");
    let mut buf = String::new();
    let mut link_index = 0;

    'outer: loop {
        buf.clear();
        match parse_line_from_reader(&mut reader, &mut buf) {
            (None, _) => {
                break 'outer
            },
            (Some(line), eof) => {
                match line {
                    NexLine::Text(text) => {
                        println!("{}", text);
                    },
                    NexLine::Link(url, title, has_title) => {
                        link_index += 1;
                        if has_title {
                            println!("[{}] {}", link_index, title);
                        } else {
                            println!("[{}] {}", link_index, url);
                        }
                    }
                }
                if eof {
                    break 'outer
                }
            },
        }
    }
}
